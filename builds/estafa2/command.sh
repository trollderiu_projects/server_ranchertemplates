
if [ -f /var/www/data/today.sql ]; then mysql -h $MYSQL_LOCATION -u $MYSQL_USERNAME -p"$MYSQL_PASSWORD" $MYSQL_DATABASE < /var/www/data/today.sql; fi

chown -R www-data:www-data /var/www/osqa/

#service apache2 start
/usr/sbin/apache2ctl -D FOREGROUND
python /var/www/osqa/manage.py runserver 0.0.0.0:8000
