#!/bin/sh

mysqldump --host=$MYSQL_LOCATION --user=$MYSQL_USERNAME --password=$MYSQL_ROOT_PASSWORD > /var/www/data/last.sql
zip mysqldump-$(date +"%Y-%m-%d").zip /var/www/data/last.sql

cp /etc/apache2/ports.conf /var/www/data/ports-$(date +"%Y-%m-%d").conf

#remove files month ago excpeting 1st day of every month
find /var/www/data/. -mtime +30 -printf '%Cd#%p\n' |grep -v '^01' |cut -d '#' -f 2
